import { Provider } from 'react-redux';
import injectTapEventPlugin from 'react-tap-event-plugin';
import React from 'react';
import ReactDOM from 'react-dom';
import 'babel-polyfill';
import configureStore from './store/configureStore';
import App from './containers/App';

const store = configureStore();

injectTapEventPlugin();

ReactDOM.render(
  <Provider store={store}>
    <div>
      <App />
    </div>
  </Provider>, document.getElementById('root'));
