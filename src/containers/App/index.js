import { Card, CardHeader, Dialog, RaisedButton } from 'material-ui';
import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import HeaderBar from '../../components/HeaderBar';
import BottomBar from '../../components/BottomBar';
import SimpleForm from '../../components/SimpleForm';
import * as sendFormToEmail from '../../actions/sendEmail';
import { styles } from './styles.scss';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.closeDialog = this.closeDialog.bind(this);
    this.dispatchSendingEmail = this.dispatchSendingEmail.bind(this);
    this.state = {
      open: false,
    };
  }
  dispatchSendingEmail = (values) => {
    this.setState({ open: true });
    this.props.sendFormToEmail(values);
  }
  closeDialog = () => {
    this.setState({ open: false });
  }
  render = () => (
    <MuiThemeProvider >
      <div>
        <HeaderBar />
        <Card >
          <CardHeader
            title={
              <div className={`${styles}`}>
                <span>
                  Below You will find examples of texts we need to publish your application.
                                    </span>
                <span>
                  Please fill all fields in this form (pay attention on the correct number of marks in every field) and click &quot;SEND&quot; on the bottom of the page
                                    </span>
              </div>}
            style={{ textAlign: 'center' }}
            textStyle={{ paddingRight: '0px' }}
          />
        </Card>
        <SimpleForm onSubmit={this.dispatchSendingEmail} />
        <Dialog
          title="Your form has been sent"
          open={this.state.open}
          actions={<RaisedButton
            label="Ok"
            primary
            onTouchTap={this.closeDialog}
          />}
        > Placeholder for eventualy content/instruction for customer
                     </Dialog>
        <BottomBar />
      </div>
    </MuiThemeProvider>
  )
}
App.propTypes = {
  sendFormToEmail: PropTypes.func.isRequired,
};

export default connect(null, dispatch => bindActionCreators({
  ...sendFormToEmail,
  ...{},
}, dispatch))(App);
