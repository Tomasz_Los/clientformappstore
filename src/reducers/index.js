const { combineReducers } = require('redux');
const { reducer: formReducer } = require('redux-form');

module.exports = combineReducers({
  form: formReducer,
});
