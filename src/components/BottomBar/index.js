import { AppBar } from 'material-ui';
import React from 'react';
import { styles } from './styles.scss';

const bottomLogo = require('../../assets/pictures/Logo1.png');

function BottomBar() {

  return (
    <AppBar
      className={`${styles}`}
      showMenuIconButton={false}
      iconElementRight={
        <div className="div-for-copyrights-and-logo" >
          <span className="span-app-bar">Copyright 2016 &copy; VR Global, Inc.</span>
          <img className="bottom-logo" alt="VR Global logo" src={bottomLogo} />
        </div>
      }
      iconStyleRight={{ margin: '2px' }}
      style={{
        backgroundColor: 'black',
      }}
    />
  );
}
export default BottomBar;
