import React from 'react';
import { AppBar } from 'material-ui';
import { styles } from './styles.scss';

const headerLogo = require('../../assets/pictures/Logo2.png');

function HeaderBar() {

  return (
    <AppBar
      title={<div className="header-main-title">VR Mobile APP</div>}
      className={`${styles}`}
      showMenuIconButton={false}
      iconElementRight={
        <img alt="VR Global Logo" className="top-logo" src={headerLogo} />
      }
    />
  );
}
export default HeaderBar;
