import React, { PropTypes } from 'react';
import { Card, CardTitle, TextField } from 'material-ui';

const cardStyles = {
  minWidth: '250px',
  marginleft: 'auto',
  marginRight: 'auto',
  padding: '0',
  display: 'block',
  boxShadow: 'none',
  border: 'none',
};
const textareaStyle = {
  border: '1px solid grey',
  marginTop: '14px',
  marginBottom: '0',
  overflow: 'hidden',
};
const floatingLabelStyle = {
  top: '15px',
  marginLeft: '5px',
};
const floatingLabelFocusStyle = {
  top: '18px',
  marginLeft: '5px',
};
const floatingLabelShrinkStyle = {
  top: '18px',
  marginLeft: '5px',
};
const errorStyle = {
  bottom: '32px',
  fontWeight: '600',
};
const cardTitleStyle = {
  border: 'none',
  padding: '0',
  marginTop: '0',
  marginBottom: '0',
  marginLeft: '10px',
};

export default function SingleTextAreaField(props) {
  const {
    input, title, maxLengthCharacters, meta: { touched, error }, ...custom
  } = props;
  return (
    <Card style={cardStyles} >
      <CardTitle
        title={props.title}
        style={cardTitleStyle}
      />
      <TextField
        floatingLabelText={
          (props.maxLengthCharacters === Infinity ? ' ' : false) ||
          (props.maxLengthCharacters ? (`${input.value.length}/${props.maxLengthCharacters}`) : ' ')
        }
        textareaStyle={textareaStyle}
        floatingLabelStyle={floatingLabelStyle}
        floatingLabelFocusStyle={floatingLabelFocusStyle}
        floatingLabelShrinkStyle={floatingLabelShrinkStyle}
        errorStyle={errorStyle}
        maxLength={props.maxLengthCharacters}
        underlineShow={false}
        errorText={touched && error}
        {...input}
        {...custom}
      />
    </Card>
  );
}
SingleTextAreaField.propTypes = {
  maxLengthCharacters: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  input: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  meta: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
};
