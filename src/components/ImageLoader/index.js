import React, { PropTypes } from 'react';
import { styles } from './styles.scss';

export default function ImageLoader(props) {
  return (
    <div className={`${styles}`}>
      <div
        className="image-cropper"
        style={{ height: props.imageLong }}
      >
        <img
          alt="example screenshot from app store"
          className="inserted-image"
          src={props.imgAdress}
          style={{ marginTop: props.cropImageFromUp }}
        />
      </div>
    </div>
  );
}

ImageLoader.propTypes = {
  imageLong: PropTypes.string.isRequired,
  imgAdress: PropTypes.string.isRequired,
  cropImageFromUp: PropTypes.string.isRequired,
};

