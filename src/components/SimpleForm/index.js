import { Field, reduxForm, reset } from 'redux-form';
import React, { PropTypes } from 'react';
import { Card, CardTitle, RaisedButton } from 'material-ui';
import { styles } from './styles.scss';
import ImageLoader from '../ImageLoader';
import SingleTextAreaField from '../SingleTextAreaField';

const required = value => (value ? undefined : 'REQUIRED!');
const titleAndDescriptionImageAndroid = require('../../assets/pictures/TitleAndShortDescriptionAndroid.png');
const longDescriptionAndroid = require('../../assets/pictures/LongDescriptionAndroid.png');
const contactInfoAndroid = require('../../assets/pictures/contactInfoAndroid.png');
const titleIOS = require('../../assets/pictures/TitleIOS.png');
const longDescriptionIOS = require('../../assets/pictures/LongDescriptionIOS.png');

const fieldStyle = { paddingTop: '0', border: 'none', width: '100%' };
const cardTitleStyle = { textAlign: 'center', padding: '1.5em 0 1.5em 0' };
const labelStyle = { fontSize: '1.5em', letterSpacing: '2px' };
const afterSubmit = (result, dispatch) => {
  dispatch(reset('simple'));
};
function SimpleForm(props) {
  const {
    handleSubmit,
    reset,  //  eslint-disable-line no-shadow
    submitting,
  } = props;

  return (
    <form onSubmit={handleSubmit} className={`${styles}`} >
      <Card className="main-card-for-android-and-ios-form" >
        <CardTitle
          title="Google Play Store"
          style={cardTitleStyle}
          titleStyle={{ fontSize: '2em' }}
          subtitle="Android"
          subtitleStyle={{ fontSize: '1.5em' }}
        />
        <div className="container-for-fields-and-images">
          <div className="container-for-fields">
            <Field
              name="appNameAndroid"
              component={SingleTextAreaField}
              maxLengthCharacters={30}
              title="Application title"
              validate={required}
              disabled={submitting}
              style={fieldStyle}
              multiLine
              rows={1}
            />
            <Field
              name="appShortDescriptionAndroid"
              component={SingleTextAreaField}
              maxLengthCharacters={80}
              title="Short Description"
              validate={required}
              style={fieldStyle}
              rows={3}
              multiLine
              disabled={submitting}
            />
          </div>
          <div className="container-for-image" >
            <ImageLoader
              imageLong={'356px'}
              cropImageFromUp={'-25px'}
              imgAdress={titleAndDescriptionImageAndroid}
            />
          </div>
        </div>
        <div className="container-for-fields-and-images">
          <div className="container-for-fields" >
            <Field
              name="appLongDescriptionAndroid"
              component={SingleTextAreaField}
              maxLengthCharacters={4000}
              title="Long Description"
              validate={required}
              style={fieldStyle}
              rows={10} multiLine
              disabled={submitting}
            />
          </div>
          <div className="container-for-image">
            <ImageLoader
              imgAdress={longDescriptionAndroid}
              imageLong={'240px'}
              cropImageFromUp={'-24px'}
            />
          </div>
        </div>
        <div className="container-for-fields-and-images">
          <div className="container-for-fields" >
            <Field
              name="contactWebsiteAndroid"
              component={SingleTextAreaField}
              maxLengthCharacters={Infinity}
              title="Contact details: website"
              validate={required}
              disabled={submitting}
              style={fieldStyle}
              multiLine
              rows={1}
            />
            <Field
              name="contactEmailAndroid"
              component={SingleTextAreaField}
              maxLengthCharacters={Infinity}
              title="Contact details: email"
              validate={required}
              disabled={submitting}
              style={fieldStyle}
              multiLine
              rows={1}
            />
            <Field
              name="contactPhoneAndroid"
              component={SingleTextAreaField}
              title="Contact details: phone"
              maxLengthCharacters={Infinity}
              validate={required}
              disabled={submitting}
              style={fieldStyle}
              multiLine
              rows={1}
            />
            <Field
              name="privacyPolicyURLAndroid"
              component={SingleTextAreaField}
              maxLengthCharacters={Infinity}
              title="Privacy policy URL"
              validate={required}
              disabled={submitting}
              style={fieldStyle}
              multiLine
              rows={1}
            />
          </div>
          <div className="container-for-image" >
            <ImageLoader
              imgAdress={contactInfoAndroid}
              imageLong={'342px'}
              cropImageFromUp={'-60px'}
            />
          </div>
        </div>
      </Card>
      <Card className="main-card-for-android-and-ios-form">
        <CardTitle
          title="App Store"
          style={cardTitleStyle}
          titleStyle={{ fontSize: '2em' }}
          subtitle="iOS"
          subtitleStyle={{ fontSize: '1.5em' }}
        />
        <div className="container-for-fields-and-images" >
          <div className="container-for-fields">
            <Field
              name="appNameIOS"
              component={SingleTextAreaField}
              maxLengthCharacters={255}
              title="Application Name"
              validate={required}
              style={fieldStyle}
              rows={3}
              multiLine
              disabled={submitting}
            />
          </div>
          <div className="container-for-image" >
            <ImageLoader
              imgAdress={titleIOS}
              imageLong={'270px'}
              cropImageFromUp={'-55px'}
            />
          </div>
        </div>
        <div className="container-for-fields-and-images">
          <div className="container-for-fields" >
            <Field
              name="appDescriptionIOS"
              component={SingleTextAreaField}
              maxLengthCharacters={400}
              title="Application Description"
              validate={required}
              style={fieldStyle}
              rows={4}
              multiLine
              disabled={submitting}
            />
          </div>
          <div className="container-for-image">
            <ImageLoader
              imgAdress={longDescriptionIOS}
              imageLong={'310px'}
              cropImageFromUp={'-45px'}
            />
          </div>
        </div>
        <CardTitle
          className="helpfull-card-title-keywords"
          title="Other information we need to fill during application deployment process:"
          titleStyle={{ fontSize: '1.5em' }}
        />
        <div className="container-for-fields-and-images" >
          <div className="container-for-fields" >
            <Field
              name="keywords"
              component={SingleTextAreaField}
              maxLengthCharacters={200}
              title="Keywords"
              validate={required}
              style={fieldStyle}
              rows={3}
              multiLine
              disabled={submitting}
            />
          </div>
          <div className="helpfull-container-for-description" >
            <div className="div-in-helpfull-container-for-description" >
              One or more keywords that describe your app. Keywords make App Store Search results more accurte
              </div>
          </div>
        </div>
      </Card>
      <div className="div-for-buttons">
        <RaisedButton
          className="button"
          label="SEND"
          primary type="submit"
          labelStyle={labelStyle}
          disabled={submitting}
        />
        <RaisedButton
          className="button"
          label="RESET"
          secondary type="button"
          onClick={reset}
          labelStyle={labelStyle}
          disabled={submitting}
        />
      </div>
    </form>
  );
}
SimpleForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
};
export default reduxForm({
  form: 'simple',
  onSubmitSuccess: afterSubmit,
})(SimpleForm);
