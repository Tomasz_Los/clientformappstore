import SENDING_EMAIL from 'types.js';

export function sendFormToEmail(values) { // eslint-disable-line import/prefer-default-export
  const emails = ['info@vrglobal.com', 'mw@vrglobal.com','pm@vrglobal.com];

  emails.forEach((value) => {
    const ourApi = (`http://api.white.vrglobal.com/v3/contactUs/${value}/Customer Mobile App (${values.appNameAndroid}) details has been send`);
    fetch(ourApi, {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-type': 'application/json',
      },
      body: JSON.stringify({
        name: 'placeholder',
        email: 'placeholder',
        offerType:
        `MOBILE APPS DETAILS
        \n\n *****[[Google Play Store - Android]]*****
            \nApp Name:\n${values.appNameAndroid} 
            \nShort description:\n${values.appShortDescriptionAndroid}
            \nLong description:\n${values.appLongDescriptionAndroid}
            \nContact Website:\n${values.contactWebsiteAndroid} 
            \nContact Email:\n${values.contactEmailAndroid} 
            \nContact Phone:\n${values.contactPhoneAndroid}
            \nPrivacy Policy URL:\n${values.privacyPolicyURLAndroid}
            \n\n\n *****[[App Store - iOS]]*****" 
            \nApp Name:\n${values.appNameIOS} 
            \nApp Description: ${values.appDescriptionIOS} 
            \nApp Keywords:\n${values.keywords}`,
      }),
    });
  });
  return {
    type: SENDING_EMAIL,
    payload: values,
  };
}
